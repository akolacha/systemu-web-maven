@echo off
set env=%~1
set xmlfile=%~1
set email=%~2
set pwd=%~3
set endpoint=%4
set env=SQA
set SOAPUI_HOME=C:\Program Files\SmartBear\SoapUI-5.2.1\bin\
echo %SOAPUI_HOME%
rem pause
set RESULT_FOLDER_NAME=Result-%date:~10,4%%date:~7,2%%date:~4,2%-%TIME:~0,2%.%TIME:~3,2%.%TIME:~6,2%

if exist "%SOAPUI_HOME%..\jre\bin" goto SET_BUNDLED_JAVA

if exist "%JAVA_HOME%" goto SET_SYSTEM_JAVA

echo JAVA_HOME is not set, unexpected results may occur.
echo Set JAVA_HOME to the directory of your local JDK to avoid this message.
goto SET_SYSTEM_JAVA

:SET_BUNDLED_JAVA
set JAVA=%SOAPUI_HOME%..\jre\bin\java
goto END_SETTING_JAVA

:SET_SYSTEM_JAVA
set JAVA=java

:END_SETTING_JAVA


rem init classpath

set CLASSPATH=%SOAPUI_HOME%soapui-5.2.1.jar;%SOAPUI_HOME%..\lib\*
"%JAVA%" -cp "%CLASSPATH%" com.eviware.soapui.tools.JfxrtLocator > %TEMP%\jfxrtpath
set /P JFXRTPATH= < %TEMP%\jfxrtpath
del %TEMP%\jfxrtpath
set CLASSPATH=%CLASSPATH%;%JFXRTPATH%

rem JVM parameters, modify as appropriate
rem set JAVA_OPTS=-Xms128m -Xmx1024m -Dsoapui.properties=CN_Lion_API_SQA_GlobalProperties.properties "-Dsoapui.home=%SOAPUI_HOME%\"

if "%SOAPUI_HOME%\" == "" goto START
    set JAVA_OPTS=%JAVA_OPTS% -Dsoapui.ext.libraries="%SOAPUI_HOME%ext"
    set JAVA_OPTS=%JAVA_OPTS% -Dsoapui.ext.listeners="%SOAPUI_HOME%listeners"
    set JAVA_OPTS=%JAVA_OPTS% -Dsoapui.ext.actions="%SOAPUI_HOME%actions"

:START
echo %JAVA_OPTS%
rem pause
rem ********* run soapui testcase runner ***********
rem pause
"%JAVA%" %JAVA_OPTS% com.eviware.soapui.tools.SoapUITestCaseRunner -G "email_id"=%email% -G "password"=%pwd% -G "endPoint"=%endpoint% -I /"%CD%\%xmlfile%/"
rem cscript "%CD%\result_Extractor.vbs"
rem cscript "%CD%\result_Extractor.vbs" %RESULT_FOLDER_NAME%
exit