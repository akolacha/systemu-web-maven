package ExcelDB;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

/**
 * Provides methods to build and execute queries.This acts a super class for any derived
 * classes that want to connect to the database or any other queryable resources using odbc 
 * drivers.
 * 
 *
 * The derived classes are expected to initialize the drivers required to perform
 * the necessary operations.
 * @author ShivaPrakash
 */

public abstract class AbstractQueryBuilder {
	
	/**
	 * Opens the connection to the resource.
	 * @param connectionString
	 * @throws SQLException
	 */
	public void open( String connectionString ) throws SQLException, IllegalArgumentException {
		if ( connectionString == null || connectionString.isEmpty() )
			throw new IllegalArgumentException("connectionString");
		
		
		if(!this.isOpen()){
			m_connection = DriverManager.getConnection(connectionString);
			System.out.println("connectionString :" + connectionString);
		}
		
		System.out.println("m_connection :" + m_connection);
	}
	
	
	/**
	 * Opens the database connection using the given url, user name and password.
	 * @param url
	 * @param userName
	 * @param pwd
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public void open( String url, String userName, String pwd ) throws SQLException, IllegalArgumentException {
		if ( url == null || url.isEmpty() )
			throw new IllegalArgumentException("url");
		if ( userName == null || userName.isEmpty() )
			throw new IllegalArgumentException("userName");
		m_connection = DriverManager.getConnection(url,userName,pwd);
	}
	
	
	/**
	 * Closes the existing connection
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		if ( m_connection != null && !m_connection.isClosed() )
			m_connection.close();
	}
	
	
	/**
	 * Returns true, if there is a connection open to the excel application.
	 * @return boolean
	 */
	public boolean isOpen() throws SQLException {
		return m_connection != null && !m_connection.isClosed();
	}
	
	
	/**
	 * Executes the query and returns a row enumeration.
	 * @param selectQuery
	 * @return Enumeration<Row>
	 * @throws SQLException
	 */
	public Enumeration<Row> executeQuery( String selectQuery ) throws SQLException {
		Statement st = m_connection.createStatement( );
		ResultSet rs = st.executeQuery( selectQuery );
		return new RowEnumeration( rs );
	}
	
	
	/**
	 * Executes the query and returns a row enumeration.
	 * @param selectQuery
	 * @return Enumeration<Row>
	 * @throws SQLException
	 */
	public ResultSet getResultSet( String selectQuery ) throws SQLException {
		Statement st = m_connection.createStatement( );
		ResultSet rs = st.executeQuery( selectQuery );
		return rs;
	}
	
	
	/**
	 * Executes any DML query.It could be an INSERT,UPDATE or DELETE query.
	 * @param query
	 * @return integer
	 * @throws SQLException
	 */
	public int executeUpdate( String query ) throws SQLException {
		Statement st = m_connection.createStatement( );
		try	{
			return st.executeUpdate( query );
		}
		finally	{
			st.close();
		}
	}
	
	/**
	 * Executes the query and returns a result set.
	 * @param selectQuery
	 * @return ResultSet
	 * @throws SQLException
	 *//*
	public ResultSet getResultSet( String selectQuery ) throws SQLException {
		Statement st = m_connection.createStatement( );
		ResultSet rs = st.executeQuery( selectQuery );
		return rs;
	}*/
	
	/**
	 * Connection to the database.
	 */
	protected Connection m_connection = null;
	
}
