package ExcelDB;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


/**
 * Represents a work sheet row in the result set.
 * It provides various methods to get the data of different data types in the cell 
 * based on the column index or column name.
 * @author ShivaPrakash
 *
 */
public class Row {
	
	/**
	 * Current row in a result set.
	 */
	int m_colCount;
	/**
	 * HashMap holding index to real value
	 */
	HashMap<Integer, Object> m_valuesByIndex;
	/**
	 * Result set.
	 */
	ResultSet m_results;
	
	protected Row( ResultSet results, int colCount ) throws SQLException{
		m_colCount = colCount;
		m_results = results;
		m_valuesByIndex = new HashMap<Integer,Object>( 3 );
		fillValues( results );
	}
	
	/**
	 * Gets the cell value using the given column name.
	 * @param colName
	 * @return Object
	 * @throws SQLException
	 */
	public Object getValueByColumnName( String colName ) throws SQLException {
		int colIndex = m_results.findColumn(colName);
		return getValueAtIndex( colIndex );
	}
	
	/**
	 * Gets the values at the specified column index
	 * @param colIndex
	 * @return Object
	 * @throws SQLException
	 */
	public Object getValueAtIndex( int colIndex )throws SQLException {
		if ( colIndex < 0 || colIndex > m_colCount )
			throw new ArrayIndexOutOfBoundsException("Column index is either negative or greater than the maximum available");
		
		return m_valuesByIndex.get(new Integer(colIndex));
	}

	/**
	 * Gets all the data in the form of string array by removing the delimiter ";" in this column data.
	 * @param colName
	 * @return String[ ]
	 * @throws SQLException
	 */
	public String[] getAllDataByColumnName( String colName ) throws SQLException {
		int colIndex = m_results.findColumn(colName);
		String strData = (String) getValueAtIndex( colIndex );
		/*if(strData.contains("\n"))
			strData = strData.replaceAll("\n", "");*/
		String[] result = strData.split("\\;");
		for(int i=0 ; i<result.length ; i++){
			System.out.println(result[i]);
		}
		return result;
	}
	
	/**
	 * Gets all the data in the form of string array by removing the delimiter ";" in this column data.
	 * @param colName
	 * @return String[ ]
	 * @throws SQLException
	 * 
	 * Added extra method to ignore the "\n" character if any and get the array excluding the "\n" character.
	 * NOTE: Basically added to get Multiple expected rows in a single array
	 */
	public String[] getAllDataByColumnName( String colName, boolean ignoreEnterKey ) throws SQLException {
		int colIndex = m_results.findColumn(colName);
		String strData = (String) getValueAtIndex( colIndex );
		if(!ignoreEnterKey){
			if(strData.contains("\n"))
				strData = strData.replaceAll("\n", "");
		}
		String[] result = strData.split("\\;");
		for(int i=0 ; i<result.length ; i++){
			System.out.println(result[i]);
		}
		return result;
	}
	
	/**
	 * Gets the value at the column index provided as the specific type.
	 * @param <T>
	 * @param cls
	 * @param colIndex
	 * @return T
	 * @throws SQLException
	 */
	public <T> T getValueAtIndex( Class<T> cls, int colIndex ) throws SQLException {
		return cls.cast( getValueAtIndex( colIndex ) );
	}
	
	/**
	 * Gets the value at the provided column index as an integer.
	 * @param colIndex
	 * @return integer
	 * @throws SQLException
	 */
	public int getAsInteger( int colIndex ) throws SQLException {
		Object value = getValueAtIndex(colIndex);
		return (int) Double.valueOf( value.toString() ).doubleValue();
	}
	
	/**
	 * Gets the value at the provided column index as an double.
	 * @param colIndex
	 * @return double
	 * @throws SQLException
	 */
	public double getAsDouble( int colIndex ) throws SQLException {
		Object value = getValueAtIndex(colIndex);
		return Double.valueOf( value.toString() ).doubleValue();
	}
	
	/**
	 * Gets the value as an integer.
	 * @param colName
	 * @return integer
	 * @throws SQLException
	 */
	public int getAsInteger( String colName ) throws SQLException {
		int colIndex = m_results.findColumn(colName);
		return getAsInteger(colIndex);
	}
	
	/**
	 * Gets the value as an double.
	 * @param colName
	 * @return double
	 * @throws SQLException
	 */
	public double getAsDouble( String colName ) throws SQLException {
		int colIndex = m_results.findColumn(colName);
		return getAsDouble(colIndex);
	}
	
	/**
	 * Gets the value as Date.
	 * @param colIndex
	 * @return Date
	 * @throws SQLException
	 */
	public java.util.Date getAsDate( int colIndex ) throws SQLException, java.text.ParseException {
		Object value = getValueAtIndex(colIndex);
		java.sql.Timestamp timeStamp = (java.sql.Timestamp) value;
		return timeStamp;
	}
	
	/**
	 * Gets the value as Date only string.
	 * @param colIndex
	 * @return String
	 * @throws SQLException
	 */
	public String getAsDateString( int colIndex ) throws SQLException, java.text.ParseException {
		return getAsDateString( colIndex, "MM/dd/yyyy" );
	}
	
	/**
	 * Gets the date as a string using the format provided.
	 * @param colIndex
	 * @param dateFormat
	 * @return String
	 * @throws SQLException
	 * @throws java.text.ParseException
	 */
	public String getAsDateString( int colIndex, String dateFormat ) throws SQLException, java.text.ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(getAsDate(colIndex));
	}
	
	/**
	 * Gets the value as Date only string.
	 * @param colName
	 * @return String
	 * @throws SQLException
	 */
	public String getAsDateString( String colName ) throws SQLException, java.text.ParseException {
		int colIndex = m_results.findColumn(colName);
		return getAsDateString( colIndex );		
	}
	
	/**
	 * Gets the date string value for the column name using the format provided.
	 * @param colName
	 * @param dateFormat
	 * @return String
	 * @throws SQLException
	 * @throws java.text.ParseException
	 */
	public String getAsDateString( String colName, String dateFormat ) throws SQLException, java.text.ParseException {
		return getAsDateString( m_results.findColumn(colName), dateFormat );	
	}
	

	/**
	 * Gets the text from the provided column and returns the string in yyyyMMdd date format.
	 * @param colName
	 * @return String
	 * @throws SQLException
	 * @throws java.text.ParseException
	 */
	public String getFormattedDateString( String colName, String format ) throws SQLException, java.text.ParseException {
		String dateText = getValueByColumnName(colName).toString();
		String formattedDate = "";
		
		if(format.equalsIgnoreCase("yyyyMMdd")){
			String date = dateText.substring(0, 2);
			String month = dateText.substring(2, 4);
			String year = "20" + dateText.substring(4);
			formattedDate = year+month+date;
		}else if(format.equalsIgnoreCase("dd-MMM-yy")){
			SimpleDateFormat informat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat outformat = new SimpleDateFormat(format);
			Date dt = informat.parse(dateText);
			formattedDate = outformat.format(dt);
		}else{
			
		}
				
		return formattedDate;
	}
	
	
	/**
	 * Gets the number of columns available in the result set.
	 * @return integer
	 */
	public int getColumnsCount(){
		return m_colCount;
	}
	
	/**
	 * Gets the column names belonging to the results.
	 * @return String[ ]
	 */
	public String[ ] getColumnNames(){
		try {
			ResultSetMetaData rsmd = m_results.getMetaData( );
			String[] colNames = new String[ m_colCount ];
			for( int index = 0; index < m_colCount; index++ ) {
				colNames[ index ] = rsmd.getColumnLabel(index + 1);
			}
			return colNames;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}
	
	/**
	 * Gets the cells belonging to the current row.
	 * @return Cell[ ]
	 * @throws SQLException
	 */
	public Cell[ ] getCells() throws SQLException {
		Cell[ ] cells = new Cell[ m_colCount ];
		int row = m_results.getRow();
		ResultSetMetaData rsmd = m_results.getMetaData( );
		for( int index = 0; index < m_colCount; index++ ) {
			int colIndex = index + 1;
			cells[ index ] = new Cell( row, colIndex, rsmd.getColumnLabel(colIndex), getValueAtIndex( colIndex ) );
		}
		return cells;
	}
	
	/**
	 * Fills the values from the current row into a HashMap.
	 * @param results
	 * @throws SQLException
	 */
	void fillValues( ResultSet results ) throws SQLException {
		for( int index = 0; index < m_colCount; index++ ){
			int colIndex = index + 1;
			Object value = results.getObject( colIndex );
			m_valuesByIndex.put( new Integer( colIndex ), value );
		}
	}
}
