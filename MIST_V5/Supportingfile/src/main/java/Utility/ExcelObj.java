
package Utility;
/*
 ********************************************************************************
 ********************************************************************************
//  TimeTrax Automation Selenium
//  Start date 7 Jan 2014
//  Author : Raghavendra Pai & Santhosha HC
//  Test :    Excel object script V 1.9 

********************************************************************************
********************************************************************************/
import static Utility.ResultExcel.cell;
import static Utility.ResultExcel.style;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.ss.usermodel.Hyperlink;


import org.apache.poi.xssf.usermodel.*; 
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelObj {
	//public static String filename = System.getProperty("user.dir")+"\\src\\config\\testcases\\TestData.xls";
	public  String Path;
	public  FileInputStream RFIS = null;
	public  FileOutputStream RFOS =null;
	public XSSFWorkbook ResWorkBook = null;
	public XSSFSheet ResSheet = null;
	private XSSFRow ResRow   =null;
	private XSSFCell ResCell = null;
	
	public  FileInputStream FIS = null;
	public  FileOutputStream FOS =null;
	private XSSFWorkbook WorkBook = null;
	public XSSFSheet Sheet = null;
	private XSSFRow Row   =null;
	private XSSFCell Cell = null;
	
	public ExcelObj(String path) {
		
		this.Path=path;
		try {
			FIS = new FileInputStream(path);
			WorkBook = new XSSFWorkbook(FIS);
			Sheet = WorkBook.getSheetAt(0);
			FIS.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	// returns the row count in a sheet
	public int getRowCount(String sheetName){
		int index = WorkBook.getSheetIndex(sheetName);
		if(index==-1)
			return 0;
		else{
		Sheet = WorkBook.getSheetAt(index);
		int number=Sheet.getLastRowNum()+1;
		return number;
		}
		
	}
	
	// returns the data from a cell
	public String getCellData(String sheetName,String colName,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = WorkBook.getSheetIndex(sheetName);
		int col_Num=-1;
		if(index==-1)
			return "";
		
		Sheet = WorkBook.getSheetAt(index);
		Row=Sheet.getRow(0);
		for(int i=0;i<Row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(Row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
				col_Num=i;
		}
		if(col_Num==-1)
			return "";
		
		Sheet = WorkBook.getSheetAt(index);
		Row = Sheet.getRow(rowNum-1);
		if(Row==null)
			return "";
		Cell = Row.getCell(col_Num);
		
		if(Cell==null)
			return "";
		//System.out.println(cell.getCellType());
		if(Cell.getCellType()==Cell.CELL_TYPE_STRING)
			  return Cell.getStringCellValue();
		else if(Cell.getCellType()==Cell.CELL_TYPE_NUMERIC || Cell.getCellType()==Cell.CELL_TYPE_FORMULA ){
			  
			  String cellText  = String.valueOf(Cell.getNumericCellValue());
			  if (HSSFDateUtil.isCellDateFormatted(Cell)) {
		           // format in form of M/D/YY
				  double d = Cell.getNumericCellValue();

				  Calendar cal =Calendar.getInstance();
				  cal.setTime(HSSFDateUtil.getJavaDate(d));
		            cellText =
		             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
		           cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
		                      cal.get(Calendar.MONTH)+1 + "/" + 
		                      cellText;
		           
		           //System.out.println(cellText);

		         }

		return cellText;
		}else if(Cell.getCellType()==Cell.CELL_TYPE_BLANK)
		      return ""; 
		  else 
			  return String.valueOf(Cell.getBooleanCellValue());
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colName +" does not exist in xls";
		}
	}
	/**
	 * 
	 * @param sheetname
	 * @return
	 */
	public boolean addSheetAndColumns(String sheetname) {

		FileOutputStream fileOut;
		try {
			Sheet sheet = WorkBook.createSheet(sheetname);
			Row row = sheet.createRow(0);
			
			Cell cell0 = row.createCell(0);
			cell0.setCellValue("TESTCASE");
			
			Cell cell1 = row.createCell(1);
			cell1.setCellValue("EXPECTED_VALUE");
			
			Cell cell2 = row.createCell(2);
			cell2.setCellValue("ACTUAL_VALUE");
			
			Cell cell3 = row.createCell(3);
			cell3.setCellValue("RESULT");
			
			Cell cell4 = row.createCell(4);
			cell4.setCellValue("OUTPUT_RESPONSE");
			
	        cell0.setCellStyle(setCellStyle("HEADER"));
	        cell1.setCellStyle(setCellStyle("HEADER"));
	        cell2.setCellStyle(setCellStyle("HEADER"));
	        cell3.setCellStyle(setCellStyle("HEADER"));
	        cell4.setCellStyle(setCellStyle("HEADER"));
			
			fileOut = new FileOutputStream(Path);
			WorkBook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public int getCellRowNum(String sheetName, String colName, String cellValue) {

		for (int i = 2; i <= getRowCount(sheetName); i++) {
			if (getCellData(sheetName, colName, i).equalsIgnoreCase(cellValue)) {
				return i;
			}
		}
		return -1;

	}

	public XSSFCellStyle setCellStyle(String result){
		XSSFCellStyle cellStyle = WorkBook.createCellStyle();
        cellStyle = WorkBook.createCellStyle();
		
		if(result.equalsIgnoreCase("PASS")){
			cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
	        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        cellStyle.setBorderTop((short) 1); // single line border
	        cellStyle.setBorderBottom((short) 1); // single line border
		}else if (result.equalsIgnoreCase("FAIL")){
			cellStyle.setFillForegroundColor(HSSFColor.RED.index);
	        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        cellStyle.setBorderTop((short) 1); // single line border
	        cellStyle.setBorderBottom((short) 1); // single line border
		}else if (result.equalsIgnoreCase("HEADER")){
			cellStyle.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
	        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        cellStyle.setBorderTop((short) 1); // single line border
	        cellStyle.setBorderBottom((short) 1); // single line border
		}else{
	        //cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        cellStyle.setBorderTop((short) 1); // single line border
	        cellStyle.setBorderBottom((short) 1); // single line border
		}
		
		return cellStyle;
	}
	// returns the data from a cell
	public String getCellData(String sheetName,int colNum,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = WorkBook.getSheetIndex(sheetName);

		if(index==-1)
			return "";
		
	
		Sheet = WorkBook.getSheetAt(index);
		Row = Sheet.getRow(rowNum-1);
		if(Row==null)
			return "";
		Cell = Row.getCell(colNum);
		if(Cell==null)
			return "";
		
	  if(Cell.getCellType()==Cell.CELL_TYPE_STRING)
		  return Cell.getStringCellValue();
	  else if(Cell.getCellType()==Cell.CELL_TYPE_NUMERIC || Cell.getCellType()==Cell.CELL_TYPE_FORMULA ){
		  
		  String cellText  = String.valueOf(Cell.getNumericCellValue());
		  if (HSSFDateUtil.isCellDateFormatted(Cell)) {
	           // format in form of M/D/YY
			  double d = Cell.getNumericCellValue();

			  Calendar cal =Calendar.getInstance();
			  cal.setTime(HSSFDateUtil.getJavaDate(d));
	            cellText =
	             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
	           cellText = cal.get(Calendar.MONTH)+1 + "/" +
	                      cal.get(Calendar.DAY_OF_MONTH) + "/" +
	                      cellText;
	           
	          // System.out.println(cellText);

	         }

		  
		  
		  return cellText;
	  }else if(Cell.getCellType()==Cell.CELL_TYPE_BLANK)
	      return "";
	  else 
		  return String.valueOf(Cell.getBooleanCellValue());
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colNum +" does not exist  in xls";
		}
	}
	
	public boolean setCellData(String sheetName,String colName,int rowNum, String data){
		try{
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<=0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		Sheet = WorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
		for(int i=0;i<Row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(Row.getCell(i).getStringCellValue().trim().equals(colName))
				colNum=i;
		}
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(12); 
		Row = Sheet.getRow(rowNum-1);
		if (Row == null)
			Row = Sheet.createRow(rowNum-1);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
	    Cell.setCellValue(data);
	    

	    FOS = new FileOutputStream(Path);

		WorkBook.write(FOS);

	    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	
	public boolean setCellData(String sheetName,String colName,int rowNum, String data,String url){
		//System.out.println("setCellData setCellData******************");
		try{
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<=0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		Sheet = WorkBook.getSheetAt(index);
		//System.out.println("A");
		Row=Sheet.getRow(0);
		for(int i=0;i<Row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(Row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(colName))
				colNum=i;
		}
		
		if(colNum==-1)
			return false;
		Sheet.autoSizeColumn(colNum); 
		Row = Sheet.getRow(rowNum-1);
		if (Row == null)
			Row = Sheet.createRow(rowNum-1);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);
			
   Cell.setCellValue(data);
	    XSSFCreationHelper createHelper = WorkBook.getCreationHelper();
//
	    //cell style for hyperlinks
	    //by default hypelrinks are blue and underlined
	    CellStyle hlink_style = WorkBook.createCellStyle();
	    XSSFFont hlink_font = WorkBook.createFont();
	    hlink_font.setUnderline(XSSFFont.U_SINGLE);
	    hlink_font.setColor(IndexedColors.BLUE.getIndex());
	    hlink_style.setFont(hlink_font);
	    hlink_style.setWrapText(true);

	    //C://SeleniumAutomationResult//Screen_Shots//05_18_2014-9_45_50 PM.jpg

	    XSSFHyperlink link = createHelper.createHyperlink(XSSFHyperlink.LINK_FILE);
	   //String Addr = "file:///"+url.replace("\\", "//");
	    String Addr = url.replace("\\", "/");
	    link.setAddress(Addr);
	    Cell.setHyperlink(link);
	    Cell.setCellStyle(hlink_style);
	    
	   
	    
	      
	    FOS = new FileOutputStream(Path);
		WorkBook.write(FOS);

    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	//*********************************************************************************
	
	public boolean setRowColor(String sheetName,String colName,int rowNum, String SetColor){
		try{
		
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<=0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		Sheet = WorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
		for(int i=0;i<Row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(Row.getCell(i).getStringCellValue().trim().equals(colName))
				colNum=i;
		}
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(12); 
		Row = Sheet.getRow(rowNum-1);
		if (Row == null)
			Row = Sheet.createRow(rowNum-1);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
		for (int col =0 ; col < 11 ; col++)
		{
			Cell = Row.getCell(col);	
			if (Cell == null)
		        Cell = Row.createCell(col);
		   XSSFFont font = WorkBook.createFont();
		    style = WorkBook.createCellStyle();

		    font.setBold(true);
		    font.setColor(IndexedColors.BLACK.getIndex());
		    style.setFont(font);
		    if(SetColor.equalsIgnoreCase("green"))
		    style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    style.setBorderBottom((short) 1);
		    style.setBorderLeft((short) 1);
		    style.setBorderRight((short) 1);
		    style.setBorderTop((short) 1);
		    if(SetColor.equalsIgnoreCase("red"))
			    style.setFillForegroundColor(IndexedColors.RED.getIndex());
		    
		    if(SetColor.equalsIgnoreCase("blue"))
			    style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		    
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    Cell.setCellStyle(style);
		}
		    
	    
	    FOS = new FileOutputStream(Path);

		WorkBook.write(FOS);

	    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
  
	public boolean ReslutSetCellData(String sheetName,int colNum,int rowNum, String data){
		try{
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		
		
		
		Sheet = WorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
		
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(12); 
		Row = Sheet.getRow(rowNum);
		if (Row == null)
			Row = Sheet.createRow(rowNum);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
	    Cell.setCellValue(data);
	    

	    FOS = new FileOutputStream(Path);

		WorkBook.write(FOS);

	    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}	

	
	//*********************************************************************************

    public boolean addSheet(String  sheetname){                    
        
        FileOutputStream fileOut;
        try {
                        WorkBook.createSheet(sheetname);    
                        fileOut = new FileOutputStream(Path);
                        WorkBook.write(fileOut);
             fileOut.close();                                 
        } catch (Exception e) {                                    
                        e.printStackTrace();
                        return false;
        }
        return true;
}
    
    public boolean ReslutSetRowColor(String sheetName,int colNum,int rowNum, String SetColor){
		try{
		
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		
		
		
		Sheet = WorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
	
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(12); 
		Row = Sheet.getRow(rowNum);
		if (Row == null)
			Row = Sheet.createRow(rowNum);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
		for (int col =0 ; col < 11 ; col++)
		{
			Cell = Row.getCell(col);	
			if (Cell == null)
		        Cell = Row.createCell(col);
		   XSSFFont font = WorkBook.createFont();
		    style = WorkBook.createCellStyle();

		    font.setBold(true);
		    font.setColor(IndexedColors.BLACK.getIndex());
		    style.setFont(font);
		    if(SetColor.equalsIgnoreCase("green"))
		    style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    style.setBorderBottom((short) 1);
		    style.setBorderLeft((short) 1);
		    style.setBorderRight((short) 1);
		    style.setBorderTop((short) 1);
		    if(SetColor.equalsIgnoreCase("red"))
			    style.setFillForegroundColor(IndexedColors.RED.getIndex());
		    if(SetColor.equalsIgnoreCase("yellow"))		    	
			    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		    
		    if(SetColor.equalsIgnoreCase("blue"))
			    style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		    
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    Cell.setCellStyle(style);
		}
		    
	    
	    FOS = new FileOutputStream(Path);

		WorkBook.write(FOS);

	    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
  //*************************************************************************************************
    public boolean ReslutSummarySetRowColor(String sheetName,int colNum,int rowNum, String SetColor){
		try{
		
		FIS = new FileInputStream(Path); 
		WorkBook = new XSSFWorkbook(FIS);

		if(rowNum<0)
			return false;
		
		int index = WorkBook.getSheetIndex(sheetName);
		
		
		
		Sheet = WorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
	
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(26); 
		Row = Sheet.getRow(rowNum);
		if (Row == null)
			Row = Sheet.createRow(rowNum);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
		for (int col =0 ; col < 7 ; col++)
		{
			Cell = Row.getCell(col);	
			if (Cell == null)
		        Cell = Row.createCell(col);
		   XSSFFont font = WorkBook.createFont();
		    style = WorkBook.createCellStyle();

		    font.setBold(true);
		    font.setColor(IndexedColors.BLACK.getIndex());
		    style.setFont(font);
		    if(SetColor.equalsIgnoreCase("green"))
		    style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    style.setBorderBottom((short) 1);
		    style.setBorderLeft((short) 1);
		    style.setBorderRight((short) 1);
		    style.setBorderTop((short) 1);
		    if(SetColor.equalsIgnoreCase("red"))
			    style.setFillForegroundColor(IndexedColors.RED.getIndex());
		    if(SetColor.equalsIgnoreCase("yellow"))		    	
			    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		    
		    if(SetColor.equalsIgnoreCase("blue"))
			    style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		    
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    Cell.setCellStyle(style);
		}
		    
	    
	    FOS = new FileOutputStream(Path);

		WorkBook.write(FOS);

	    FOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    
    public int getColumnCount(String sheetName){
		// check if sheet exists
		if(!isSheetExist(sheetName))
		 return -1;
		
		Sheet = WorkBook.getSheet(sheetName);
		Row = Sheet.getRow(0);
		
		if(Row==null)
			return -1;
		
		return Row.getLastCellNum();
		
		
		
	}
    public boolean isSheetExist(String sheetName){
		int index = WorkBook.getSheetIndex(sheetName);
		if(index==-1){
			index=WorkBook.getSheetIndex(sheetName.toUpperCase());
				if(index==-1)
					return false;
				else
					return true;
		}
		else
			return true;
	}
    
    public boolean NewReslutSummarySetRowColor(String sheetName,int colNum,int rowNum, String SetColor){
		try{
		if (RFIS == null)
		{
		  RFIS = new FileInputStream(ResultExcel.ResultExlFilepath); 
		  ResWorkBook = new XSSFWorkbook(RFIS);
		}
		if(rowNum<0)
			return false;
		
		int index = ResWorkBook.getSheetIndex(sheetName);
		
		
		
		Sheet = ResWorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
	
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(26); 
		Row = Sheet.getRow(rowNum);
		if (Row == null)
			Row = Sheet.createRow(rowNum);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
		for (int col =0 ; col < 7 ; col++)
		{
			Cell = Row.getCell(col);	
			if (Cell == null)
		        Cell = Row.createCell(col);
		   XSSFFont font = ResWorkBook.createFont();
		    style = ResWorkBook.createCellStyle();

		    font.setBold(true);
		    font.setColor(IndexedColors.BLACK.getIndex());
		    style.setFont(font);
		    if(SetColor.equalsIgnoreCase("green"))
		    style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    style.setBorderBottom((short) 1);
		    style.setBorderLeft((short) 1);
		    style.setBorderRight((short) 1);
		    style.setBorderTop((short) 1);
		    if(SetColor.equalsIgnoreCase("red"))
			    style.setFillForegroundColor(IndexedColors.RED.getIndex());
		    if(SetColor.equalsIgnoreCase("yellow"))		    	
			    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		    
		    if(SetColor.equalsIgnoreCase("blue"))
			    style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		    
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    Cell.setCellStyle(style);
		}
		    
	    
		if (RFOS==null)
		     RFOS = new FileOutputStream(ResultExcel.ResultExlFilepath);

		 //RFOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    
    public boolean ResultsetCellData(String sheetName,String colName,int rowNum, String data){
		try{
			if (RFIS == null)
			{
			  RFIS = new FileInputStream(ResultExcel.ResultExlFilepath); 
			  ResWorkBook = new XSSFWorkbook(RFIS);
			}

		if(rowNum<=0)
			return false;
		
		int index = ResWorkBook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		Sheet = ResWorkBook.getSheetAt(index);
		

		Row=Sheet.getRow(0);
		for(int i=0;i<Row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(Row.getCell(i).getStringCellValue().trim().equals(colName))
				colNum=i;
		}
		if(colNum==-1)
			return false;

		Sheet.setDefaultColumnWidth(12); 
		Row = Sheet.getRow(rowNum-1);
		if (Row == null)
			Row = Sheet.createRow(rowNum-1);
		
		Cell = Row.getCell(colNum);	
		if (Cell == null)
	        Cell = Row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
	    Cell.setCellValue(data);
	    
	 // if (RFOS==null)
	     RFOS = new FileOutputStream(ResultExcel.ResultExlFilepath);

	    ResWorkBook.write(RFOS);

	    RFOS.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
public List<String> getColumnNames(String sheetName){
		
		List<String> colNames = new ArrayList<String>();
		
		// check if sheet exists
		if(!isSheetExist(sheetName))
		 return null ;
		
		Sheet = WorkBook.getSheet(sheetName);
		Row = Sheet.getRow(0);
		
		if(Row==null)
			return null;
		
		for(Cell cll : Row){
			colNames.add(cll.getStringCellValue());
		}
		
		return colNames;
		
	}
	
}

